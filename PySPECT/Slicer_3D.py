from __future__ import print_function

import numpy as np
import matplotlib.pyplot as plt

class _Viewer_Building(object):
    def __init__(self, ax, X, axis):
        self.ax = ax
        ax.set_title('use scroll wheel to navigate images')

        self.X = X
        rows, cols, self.slices = X.shape
        self.ind = self.slices/2

        self.axis = axis
        if self.axis == 0:
            self.im = ax.imshow(self.X[int(self.ind), :, :])
            self.update()
        elif self.axis == 1:
            self.im = ax.imshow(self.X[:, int(self.ind), :])
            self.update()
        elif self.axis == 2:
            self.im = ax.imshow(self.X[:, :, int(self.ind)])
            self.update()
        else:
            print("Error: only args possible are 1,2,3")
            print("")

    def onscroll(self, event):
        if event.button == 'up':
            self.ind = (self.ind + 1) % self.slices
        else:
            self.ind = (self.ind - 1) % self.slices
        self.update()

    def update(self):
        if self.axis == 0:
            self.im.set_data(self.X[int(self.ind),:,:])
            self.ax.set_ylabel('slice %s' % int(self.ind))
            self.im.axes.figure.canvas.draw()
        if self.axis == 1:
            self.im.set_data(self.X[:,int(self.ind),:])
            self.ax.set_ylabel('slice %s' % int(self.ind))
            self.im.axes.figure.canvas.draw()
        if self.axis == 2:
            self.im.set_data(self.X[:,:,int(self.ind)])
            self.ax.set_ylabel('slice %s' % int(self.ind))
            self.im.axes.figure.canvas.draw()

class Viewer(object):
    def __init__(self, Data):
        self._Data = Data

    def Start(self,axis):
        def handle_close(evt):
            print("... closed")
            print("")
        print("View Slices...")
        fig, ax = plt.subplots(1, 1)
        tracker = _Viewer_Building(ax, self._Data, axis)

        fig.canvas.mpl_connect('close_event', handle_close)
        fig.canvas.mpl_connect('scroll_event', tracker.onscroll)

        plt.show()
