from __future__ import division
import numpy as np
import skimage.transform

class Backprojection(object):
    """docstring for Sino_2_Reco."""
    def __init__(self, sino_3d):
        super(Backprojection, self).__init__()

        self._sino = sino_3d
        self._dim_normal = np.size(self._sino,1)
        self._dim_tangential = np.size(self._sino,2)
        self._views = np.size(self._sino,0)
        self.output = np.zeros((self._dim_tangential,self._dim_tangential,self._dim_normal))

    def _reconstruct_slice(self,slice):

        sino_slice = self._sino[:,slice,:]
        reco_sino_slice = np.zeros((self._dim_tangential,self._dim_tangential))

        for i in range(self._views):
            angle = 360/self._views*i
            sino_slice_sel = sino_slice[i,:]
            sino_slice_sel_tiled = np.tile(sino_slice_sel,(self._dim_tangential,1))
            sino_slice_sel_tiled_rot = skimage.transform.rotate(sino_slice_sel_tiled,angle,order=3)
            reco_sino_slice += sino_slice_sel_tiled_rot

        return reco_sino_slice

    def _generate_3d_reconstructed_image(self):
        for slice in range(self._dim_normal):
            reco_slice = self._reconstruct_slice(slice)
            self.output[:,:,slice] += reco_slice

    def Init(self):
        print("reconstruction...")
        self._generate_3d_reconstructed_image()
        print("... reconstruction done")
        print("")

class Osem(object):
    """docstring for Osem."""
    def __init__(self, sino_3d, iterations, subsets):
        super(Osem, self).__init__()

        self._sino = sino_3d
        self._dim_normal = np.size(self._sino,1)
        self._dim_tangential = np.size(self._sino,2)
        self._views = np.size(self._sino,0)
        self._system_matrix = np.ones((self._dim_tangential,self._dim_tangential))
        self._iterations = iterations
        self._subsets = subsets
        self._views_indexing = range(self._views)
        self.output = np.zeros((self._dim_tangential,self._dim_tangential,self._dim_normal))

    def _process_slice(self,slice):
        tmp = np.zeros((self._dim_tangential,self._dim_tangential))
        estimate = np.ones((self._dim_tangential,self._dim_tangential))
        for i in range(self._iterations):
            for j in range(self._subsets):
                for k in self._views_indexing[j::self._subsets]:
                    p = self._sino[k,slice,:]
                    fwp = np.sum(skimage.transform.rotate(estimate,360/self._views*k,order=3),0)
                    fwp[fwp == 0] = 1
                    comp = p / fwp
                    bp = np.tile(comp,(self._dim_tangential,1))
                    tmp += skimage.transform.rotate(bp,360/self._views*k,order=3)
                estimate = estimate * tmp
                tmp *= 0

        return estimate

    def Init(self):
        for slice in range(self._dim_normal):
            reco_slice = self._process_slice(slice)
            self.output[:,:,slice] += reco_slice
