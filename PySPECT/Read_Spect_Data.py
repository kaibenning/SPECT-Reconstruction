import os
import struct
import numpy as np

#which dimension of the projection is tangential to the rotation vector?

class Read_Sin(object):
    """docstring for Read_Sin."""
    def __init__(self, dimension, views, path):
        super(Read_Sin, self).__init__()
        self._dimension = dimension
        self._views = views
        self._path_list = ""
        self._path = path
        self.output = np.zeros((views,dimension,dimension))

    def _read_binary_output(self,filename,dim,views):
        #open
        myfile = open(filename, 'rb')
        data = myfile.read()
        #get views number
        actual_views = len(data)/128**2/2
        #little end
        fmt = '<' + str(int(dim**2*actual_views)) + 'H'
        pix = struct.unpack(fmt, data)
        pix2 = np.array(pix)
        pix3 = pix2.reshape(int(actual_views),int(dim),int(dim))
        #pad with zeros
        numbers_of_zeros_to_pad = views-actual_views
        npad = ((0, int(numbers_of_zeros_to_pad)), (0, 0), (0, 0))
        pix4 = np.pad(pix3, pad_width=npad, mode='constant', constant_values=0)
        return pix4

    def _get_sin_files_in_dir(self):
        #get current path and search for ".sin" files
        path_list = []
        for file in os.listdir(self._path):
            if file.endswith(".sin"):
                path_list.append(file)
        return path_list

    def _merge_files(self,path_list,dimension,views):
        #additive merging
        for i in self._path_list:
            self.output += self._read_binary_output(i,self._dimension,self._views)
        return 0

    def Init(self):
        self._path_list = self._get_sin_files_in_dir()
        print("")
        print("Read from following files:")
        print(self._path_list)
        print("from: " + self._path)
        print("")
        print("Dimension of projections")
        print(self._dimension)
        print("")
        print("Number of projections")
        print(self._views)
        print("")
        print("processing input...")
        merge_check = self._merge_files(self._path_list,self._dimension,self._views)
        if merge_check == 0:
            print("... successfully")
            print("... (Index 0: Phi, Index 1: normal, Index 2: tangential)")
        else:
            print("... something went wrong")
        print("")
