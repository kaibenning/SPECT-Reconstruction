import numpy as np

class Projection_2_Sino(object):
    """docstring for Projection_2_Sino."""
    def __init__(self, projections):
        super(Projection_2_Sino, self).__init__()
        self._projections = projections
        self._dim = np.size(self._projections,1)
        self._views = np.size(projections,0)
        self._filter = np.ones(self._dim)
        self.output = np.zeros((self._views,self._dim,self._dim))

    def _gen_3d_sino(self):
        self.output = self._projections

    def _compute_ramp_filter(self):
        self._filter = abs(np.linspace(-1,1,self._dim))
        return 0

    def Init(self):
        self._gen_3d_sino()

    def Choose_Filter(self,filtertype):
        if filtertype == "ramp":
            self._compute_ramp_filter()
            print("ramp-filter")
            print("")

    def Filtering(self):
        print("filtering...")
        for i in range(self._views):
            for j in range(self._dim):
                #filtering must happen with Phi and Normal fixed!
                tmp = np.fft.fftshift(np.fft.fft(self.output[i,j,:]))*self._filter
                tmp2 = np.fft.ifft(np.fft.ifftshift(tmp))
                self.output[i,j,:] = tmp2.real
        print("... filtering done")
        print("")
