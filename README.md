# SPECT_Reco_from_GATE

1. Execute the Simulation
2. Copy the .sin - File to the enclosing folder
3. You may adjust the views, dimension, subset-number, iteration-number in the main.py
4. Uncomment the OSEM - or the FBP - Function in the main.py - file
5. Start the main.py - file with Python2

## Modules needed

1. numpy
2. skimage
3. matplotlib