from __future__ import print_function

import os
import numpy as np

from PySPECT import Read_Spect_Data as RDS
from PySPECT import Gen_Sino as GS
from PySPECT import Reco as R
from PySPECT import Slicer_3D as S3D

def demo_fbp(dim = 128, views = 36):

    path = os.getcwd()
    projections = RDS.Read_Sin(dim,views,path)
    projections.Init()

    sino_3d = GS.Projection_2_Sino(projections.output)
    sino_3d.Init()
    sino_3d.Choose_Filter("ramp")
    sino_3d.Filtering()

    reconstruction = R.Backprojection(sino_3d.output)
    reconstruction.Init()

    slicer = S3D.Viewer(reconstruction.output)
    slicer.Start(2)
    slicer.Start(1)

def demo_osem(dim = 128, views = 36, iterations = 2, subsets = 2):
    
    path = os.getcwd()
    projections = RDS.Read_Sin(dim,views,path)
    projections.Init()

    sino_3d = GS.Projection_2_Sino(projections.output)
    sino_3d.Init()

    reconstruction = R.Osem(sino_3d.output,iterations,subsets)
    reconstruction.Init()

    slicer = S3D.Viewer(reconstruction.output)
    slicer.Start(2)
    slicer.Start(1)

#demo_fbp()
demo_osem()
